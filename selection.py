#!/usr/bin/env python3

import pandas as pd
import numpy as np

dates = pd.date_range("20130101", periods=6)
df = pd.DataFrame(np.random.randn(6,4), index=dates, columns=list("ABCD"))
print("""
dates = pd.date_range('20130101', periods=6)
""" + str(dates))
print("""
df = pd.DataFrame(np.random.randn(6,4), index=dates, columns=list('ABCD'))
""" + str(df))

# Note:
#   While standard Python / Numpy expressions for
#   selecting and setting are intuitive and come in
#   handy for interactive work, for production code,
#   we recommend the optimized pandas data access
#   methods, `.at`, `.iat`, `.loc`, and `.iloc`

# === Getting ===

# Selecting a single column, which yields a `Series`, equivalent to `df.A`:
print("""
df['A']
""" + str(df["A"]))

# Selecting via `[]`, which slices the rows:
print("""
df[0:3]
""" + str(df[0:3]))
print("""
df['20130102':'20130104']
""" + str(df["20130102":"20130104"]))

# === Selection by Label ===

# For getting a cross section using a label:
print("""
df.loc[dates[0]]
""" + str(df.loc[dates[0]]))

# Selecting on a multi-axis by label:
print("""
df.loc[:,['A','B']]
""" + str(df.loc[:,["A","B"]]))

# Showing label slicing, both endpoints are included:
print("""
df.loc['20130102':'20130104',['A','B']]
""" + str(df.loc["20130102":"20130104",["A","B"]]))

# Reduction in the dimensions of the returned object:
print("""
df.loc['20130102',['A','B']]
""" + str(df.loc["20130102",["A","B"]]))

# For getting a scalar value:
print("""
df.loc[dates[0],'A']
""" + str(df.loc[dates[0],"A"]))

# For getting fast access to a scalar (equivalent to the previous method)
print("""
df.at[dates[0],'A']
""" + str(df.at[dates[0],"A"]))

# === Selection by Position ===

# Select via the position of the passed integers:
print("""
df.iloc[3]
""" + str(df.iloc[3]))

# By integer slices, acting similar to numpy/python:
print("""
df.iloc[3:5,0:2]
""" + str(df.iloc[3:5,0:2]))

# By lists of integer position locations, similar to the numpy/python style:
print("""
df.iloc[[1,2,4],[0,2]]
""" + str(df.iloc[[1,2,4],[0,2]]))

# For slicing rows explicitly:
print("""
df.iloc[1:3,:]
""" + str(df.iloc[1:3,:]))

# For slicing columns explicitly:
print("""
df.iloc[:,1:3]
""" + str(df.iloc[:,1:3]))

# For getting a value explicitly:
print("""
df.iloc[1,1]
""" + str(df.iloc[1,1]))

# For getting fast access to a scalar (equivalent to the previous method):
print("""
df.iat[1,1]
""" + str(df.iat[1,1]))

# === Boolean Indexing ===

# Using a single column's values to select data:
print("""
df[df.A > 0]
""" + str(df[df.A > 0]))

# Selecting values from a DataFrame where a boolean condition is met:
print("""
df[df > 0]
""" + str(df[df > 0]))

# Using the `isin()` method for filtering:
df2 = df.copy()
df2['E'] = ['one','one','two','three','four','three']
print("""
df2 = df.copy()
df2['E'] = ['one','one','two','three','four','three'])
df2
""" + str(df2) + """

df2[df2['E'].isin(['two','four'])]
""" + str(df2[df2['E'].isin(['two','four'])]))

# === Setting ===

# Setting a new column automatically aligns the data by the indexes:
s1 = pd.Series([1,2,3,4,5,6], index=pd.date_range("20130102", periods=6))
df["F"] = s1
print("""
s1 = pd.Series([1,2,3,4,5,6], index=pd.date_range('20130102', periods=6))
""" + str(s1) + """

df['F'] = s1
""" + str(df))

# Setting values by label:
df.at[dates[0],"A"] = 0
print("""
df.at[dates[0],'A'] = 0
""" + str(df))

# Setting values by position:
df.iat[0,1] = 0
print("""
df.iat[0,1] = 0
""" + str(df))

# Setting by assigning with a NumPy array:
df.loc[:,"D"] = np.array([5] * len(df))
print("""
df.loc[:,'D'] = np.array([5] * len(df))
""" + str(df))

# A `where` operation with setting:
df2 = df.copy()
print("""
df2 = df.copy()
""" + str(df2))

df2[df2 > 0] = -df2
print("""
df2[df2 > 0] = -df2
""" + str(df2))