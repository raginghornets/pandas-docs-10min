#!/usr/bin/env python3

import pandas as pd
import numpy as np

ts = pd.Series(np.random.randn(1000), index=pd.date_range('1/1/2000', periods=1000))
ts = ts.cumsum()

df = pd.DataFrame(np.random.randn(1000, 4), index=ts.index, columns=['A', 'B', 'C', 'D'])
df = df.cumsum()

# === CSV ===

# Writing to a csv file:
df.to_csv("foo.csv")
print("df.to_csv('foo.csv')")

# Reading from a csv file:
print("""
pd.read_csv('foo.csv')
""" + str(pd.read_csv("foo.csv")))

# === HDF5 ===
# Reading and writing to HDFStores

# Writing to a HDF5 store:
# df.to_hdf("foo.h5", "df")
# print("df.to_hdf('foo.h5', 'df')")

# Reading from a HDF5 Store:
# print("""
# pd.read_hdf('foo.h5','df')
# """ + pd.read_hdf("foo.h5","df"))

# === Excel ===
# Reading and writing to MS Excel

# Writing to an excel file:
# df.to_excel("foo.xlsx", sheet_name="Sheet1")
# print("df.to_excel('foo.xlsx', sheet_name='Sheet1'")

# Reading from an excel file:
# print("""
# pd.read_excel('foo.xlsx', 'Sheet1', index_col=None, na_values=['NA'])
# """ + str(pd.read_excel('foo.xlsx', 'Sheet1', index_col=None, na_values=['NA'])))