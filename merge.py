#!/usr/bin/env python3

import pandas as pd
import numpy as np

# === Concat ===

# Concatenating pandas objects together with `concat()`:
df = pd.DataFrame(np.random.randn(10, 4))
print("""
df = pd.DataFrame(np.random.randn(10, 4))
""" + str(df))

# Break it into pieces
pieces = [df[:3], df[3:7], df[7:]]
print("""
pieces = [df[:3], df[3:7], df[7:]]
""" + str(pieces) + """

pd.concat(pieces)
""" + str(pd.concat(pieces)))

# === Join ===
left = pd.DataFrame({"key": ["foo", "foo"], "lval": [1, 2]})
right = pd.DataFrame({"key": ["foo", "foo"], "rval": [4, 5]})

print("""
left = pd.DataFrame({'key': ['foo', 'foo'], 'lval': [1, 2]})
""" + str(left) + """

right = pd.DataFrame({'key': ['foo', 'foo'], 'rval': [4, 5]})
""" + str(right) + """

pd.merge(left, right, on='key')
""" + str(pd.merge(left, right, on='key')))

# Another example that can be given is:
left = pd.DataFrame({"key": ["foo", "bar"], "lval": [1, 2]})
right = pd.DataFrame({"key": ["foo", "bar"], "rval": [4, 5]})

print("""
left = pd.DataFrame({'key': ['foo', 'bar'], 'lval': [1, 2]})
""" + str(left) + """

right = pd.DataFrame({'key': ['foo', 'bar'], 'rval': [4, 5]})
""" + str(right) + """

pd.merge(left, right, on='key')
""" + str(pd.merge(left, right, on='key')))

# === Append ===

# Append rows to a dataframe
df = pd.DataFrame(np.random.randn(8, 4), columns=["A","B","C","D"])
s = df.iloc[3]
print("""
df = pd.DataFrame(np.random.randn(8, 4), columns=['A','B','C','D'])
""" + str(df) + """

s = df.iloc[3]
""" + str(s) + """

df.append(s, ignore_index=True)
""" + str(df.append(s, ignore_index=True)))