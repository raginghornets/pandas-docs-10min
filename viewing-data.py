#!/usr/bin/env python3

import pandas as pd
import numpy as np

dates = pd.date_range("20130101", periods=6)
print("""
dates = pd.date_range('20130101', periods=6)
""" + str(dates))

df = pd.DataFrame(np.random.randn(6,4), index=dates, columns=list("ABCD"))
print("""
df = pd.DataFrame(np.random.randn(6,4), index=dates, columns=list('ABCD'))
""" + str(df))

# Here is how to view the top and bottom rows of the frame
print("""
df.head()
""" + str(df.head()))
print("""
df.tail(3)
""" + str(df.tail(3)))

# Display the index, column, and the underlying NumPy data:
print("""
df.index
""" + str(df.index))
print("""
df.columns
""" + str(df.columns))
print("""
df.values
""" + str(df.values))

# `describe()` shows a quick statistic summary of your data:
print("""
df.describe()
""" + str(df.describe()))

# Transposing your data:
print("""
df.T
""" + str(df.T))

# Sorting by an axis:
print("""
df.sort_index(axis=1, ascending=False)
""" + str(df.sort_index(axis=1, ascending=False)))

# Sorting by values:
print("""
df.sort_values(by='B')
""" + str(df.sort_values(by="B")))