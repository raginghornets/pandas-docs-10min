#!/usr/bin/env python3

import pandas as pd
import numpy as np

dates = pd.date_range("20130101", periods=6)
print("""
dates = pd.date_range('20130101', periods=6)
""" + str(dates))

df = pd.DataFrame(np.random.randn(6,4), index=dates, columns=list("ABCD"))
print("""
df = pd.DataFrame(np.random.randn(6,4), index=dates, columns=list('ABCD'))
""" + str(df))

# Reindexing allows you to change/add/delete the index on a specified axis. This returns a copy of the data.
df1 = df.reindex(index=dates[0:4], columns=list(df.columns) + ["E"])
print("""
df1 = df.reindex(index=dates[0:4], columns=list(df.columns) + ['E'])
""" + str(df1))

df1.loc[dates[0]:dates[1],"E"] = 1
print("""
df1.loc[dates[0]:dates[1],'E'] = 1
""" + str(df1))

# To drop any rows that have missing data:
print("""
df1.dropna(how='any')
""" + str(df1.dropna(how="any")))

# Filling missing data:
print("""
df1.fillna(value=5)
""" + str(df1.fillna(value=5)))

# To get the boolean mask where values are `nan`:
print("""
pd.isna(df1)
""" + str(pd.isna(df1)))