#!/usr/bin/env python3

import pandas as pd
import numpy as np

# === Stack ===

tuples = list(
  zip(*[
    ["bar", "bar", "baz", "baz", "foo", "foo", "qux", "qux"],
    ["one", "two", "one", "two", "one", "two", "one", "two"]
  ])
)
index = pd.MultiIndex.from_tuples(tuples, names=["first", "second"])
df = pd.DataFrame(np.random.randn(8, 2), index=index, columns=["A", "B"])
df2 = df[:4]
print("""
tuples = list(
  zip(*[
    ["bar", "bar", "baz", "baz", "foo", "foo", "qux", "qux"],
    ["one", "two", "one", "two", "one", "two", "one", "two"]
  ])
)
""" + str(tuples) + """

index = pd.MultiIndex.from_tuples(tuples, names=['first', 'second'])
""" + str(index) + """

df = pd.DataFrame(np.random.randn(8, 2), index=index, columns=['A', 'B'])
""" + str(df) + """

df2 = df[:4]
""" + str(df2))

# The `stack()` method "compresses" a level in the DataFrame's columns:
stacked = df2.stack()
print("""
stacked = df2.stack()
""" + str(stacked))

# With a "stacked" DataFrame or Series (having a `MultiIndex` 
# as the `index`), the inverse operation of `stack()` is 
# `unstack()`, which by default unstacks the *last level*
print("""
stacked.unstack()
""" + str(stacked.unstack()) + """

stacked.unstack(1)
""" + str(stacked.unstack(1)) + """

stacked.unstacked(0)
""" + str(stacked.unstack(0)))

# === Pivot Tables ===
df = pd.DataFrame({
  "A": ["one", "one", "two", "three"] * 3,
  "B": ["A", "B", "C"] * 4,
  "C": ["foo", "foo", "foo", "bar", "bar", "bar"] * 2,
  "D": np.random.randn(12),
  "E": np.random.randn(12)
})
print("""
df = pd.DataFrame({
  "A": ["one", "one", "two", "three"] * 3,
  "B": ["A", "B", "C"] * 4,
  "C": ["foo", "foo", "foo", "bar", "bar", "bar"] * 2,
  "D": np.random.randn(12),
  "E": np.random.randn(12)
})
""" + str(df))

# We can produce pivot tables from this data very easily:
print("""
pd.pivot_table(df, values='D', index=['A', 'B'], columns=['C'])
""" + str(pd.pivot_table(df, values="D", index=["A", "B"], columns=["C"])))

# === Time Series ===
# pandas has a simple, powerful, and efficient functionality for performing
# resampling operations during frequency conversion (e.g., converting secondly
# data into 5-minutely data). This is extremely common in, but not limited to,
# financial applications.

rng = pd.date_range("1/1/2012", periods=100, freq="S")
ts = pd.Series(np.random.randint(0, 500, len(rng)), index=rng)
print("""
rng = pd.date_range('1/1/2012', periods=100, freq='S')
""" + str(rng) + """

ts = pd.Series(np.random.randint(0, 500, len(rng)), index=rng)
""" + str(ts) + """

ts.resample('5Min').sum()
""" + str(ts.resample("5Min").sum()))

# Time zone representation:
rng = pd.date_range("3/6/2012 00:00", periods=5, freq="D")
ts = pd.Series(np.random.randn(len(rng)), rng)
ts_utc = ts.tz_localize("UTC")
print("""
rng = pd.date_range('3/6/2012 00:00', periods=5, freq='D')
""" + str(rng) + """

ts = pd.Series(np.random.randn(len(rng)), rng)
""" + str(ts) + """

ts_utc = ts.tz_localize('UTC')
""" + str(ts_utc))

# Converting to another time zone:
print("""
ts_utc.tz_convert('US/Eastern')
""" + str(ts_utc.tz_convert("US/Eastern")))

# Converting between time span representations:
rng = pd.date_range("1/1/2012", periods=5, freq="M")
ts = pd.Series(np.random.randn(len(rng)), index=rng)
ps = ts.to_period()
print("""
rng = pd.date_range('1/1/2012', periods=5, freq='M')
""" + str(rng) + """

ts = pd.Series(np.random.randn(len(rng)), index=rng)
""" + str(ts) + """

ps = ts.to_period()
""" + str(ps) + """

ps.to_timestamp()
""" + str(ps.to_timestamp()))

# Converting between period and timestamp enables some convenient arithmetic
# functions to be used. In the following example, we convert a quarterly
# frequency with year ending in November to 9am of the end of the month
# following the quarter end:
prng = pd.period_range("1990Q1", "2000Q4", freq="Q-NOV")
ts = pd.Series(np.random.randn(len(prng)), prng)
print("""
prng = pd.period_range('1990Q1', '2000Q4', freq='Q-NOV')
""" + str(prng) + """

ts = pd.Series(np.random.randn(len(prng)), prng)
""" + str(ts))

ts.index = (prng.asfreq("M", "e") + 1).asfreq("H", "s") + 9
print("""
ts.index = (prng.asfreq('M', 'e') + 1).asfreq('H', 's') + 9
""" + str(ts.index) + """

ts.head()
""" + str(ts.head()))