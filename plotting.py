#!/usr/bin/env python3

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

ts = pd.Series(np.random.randn(1000), index=pd.date_range("1/1/2000", periods=1000))
print("""
ts = pd.Series(np.random.randn(1000), index=pd.date_range('1/1/2000', periods=1000))
""" + str(ts))

ts = ts.cumsum()
print("""
ts = ts.cumsum()
""" + str(ts) + """

ts.plot()
""" + str(ts.plot()))

# On a DataFrame, the `plot()` method is a convenience to plot all of the columns with labels:
df = pd.DataFrame(np.random.randn(1000, 4), index=ts.index, columns=["A", "B", "C", "D"])
print("""
df = pd.DataFrame(np.random.randn(1000, 4), index=ts.index, columns=['A', 'B', 'C', 'D'])
""" + str(df))

df = df.cumsum()
print("""
df = df.cumsum()
""" + str(df) + """

plt.figure()
""" + str(plt.figure()) + """

df.plot()
""" + str(df.plot()) + """

plt.legend(loc='best')
""" + str(plt.legend(loc="best")))

plt.show()