#!/usr/bin/env python3

import pandas as pd
import numpy as np

# pandas can include categorical data in a `DataFrame`
df = pd.DataFrame({
  "id": [1,2,3,4,5,6],
  "raw_grade": ["a", "b", "b", "a", "a", "e"]
})
print("""
df = pd.DataFrame({
  "id": [1,2,3,4,5,6],
  "raw_grade": ['a', 'b', 'b', 'a', 'a', 'e']
})
""" + str(df))

# Convert the raw grades to a categorical data type:
df["grade"] = df["raw_grade"].astype("category")
print("""
df["grade"] = df["raw_grade"].astype("category")
""" + str(df["grade"]))

# Rename the categories to more meaningful names (assigning to `Series.cat.categories` is inplace!):
df["grade"].cat.categories = ["very good", "good", "very bad"]
print("""
df["grade"].cat.categories = ["very good", "good", "very bad"]
""" + str(df["grade"].cat.categories))

# Reorder the categories and simultaneously add the missing categories (methods
# under `Series.cat` return a new `Series` by default):
df["grade"] = df["grade"].cat.set_categories(["very bad", "bad", "medium", "good", "very good"])
print("""
df["grade"] = df["grade"].cat.set_categories(["very bad", "bad", "medium", "good", "very good"])
""" + str(df["grade"]))

# Sorting is per order in the categories, not lexical order
print("""
df.sort_values(by="grade")
""" + str(df.sort_values(by="grade")))

# Grouping by a categorical column also shows empty categories
print("""
df.groupby("grade").size()
""" + str(df.groupby("grade").size()))